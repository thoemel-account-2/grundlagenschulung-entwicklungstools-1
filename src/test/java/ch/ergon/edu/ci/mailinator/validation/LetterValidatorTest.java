package ch.ergon.edu.ci.mailinator.validation;

import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class LetterValidatorTest {
    private static final int VALID_PLZ = 8400;
    private static final int INVALID_PLZ = 12345;
    public static final int ZH_PLZ = 8021;
    private final LetterValidator letterValidator = new LetterValidator();

    /**
     * Unit Test zu Aufgabe 3
     */
    @Disabled
    @Test
    public void validPlz() {
        boolean valid = letterValidator.isPlzFormatCorrect(VALID_PLZ);
        Assertions.assertThat(valid).isTrue();
    }

    /**
     * Unit Test zu Aufgabe 3
     */
    @Disabled
    @Test
    public void invalidPlz() {
        boolean valid = letterValidator.isPlzFormatCorrect(INVALID_PLZ);
        Assertions.assertThat(valid).isFalse();
    }

    /**
     * Unit Test zu Aufgabe 4
     */
    @Disabled
    @Test
    public void validContent() {
        boolean valid = letterValidator.isContentNotEmpty("abc");
        Assertions.assertThat(valid).isTrue();
    }

    /**
     * Unit Test zu Aufgabe 4
     */
    @Disabled
    @Test
    public void emptyContentIsInvalid() {
        boolean valid = letterValidator.isContentNotEmpty("");
        Assertions.assertThat(valid).isFalse();
    }

    /**
     * Unit Test zu Aufgabe 4
     */
    @Disabled
    @Test
    public void nullContentIsInvalid() {
        boolean valid = letterValidator.isContentNotEmpty(null);
        Assertions.assertThat(valid).isFalse();
    }

    /**
     * Unit Test zu Aufgabe 5
     */
    @Disabled
    @Test
    public void validCity() {
        boolean valid = letterValidator.isValidCity("Neuenburg");
        Assertions.assertThat(valid).isTrue();
    }

    /**
     * Unit Test zu Aufgabe 5
     */
    @Disabled
    @Test
    public void invalidCity() {
        boolean valid = letterValidator.isValidCity("Hinterpfupfikon");
        Assertions.assertThat(valid).isFalse();
    }

    /**
     * Unit Test zu Aufgabe 5
     */
    @Disabled
    @Test
    public void invalidEmptyCity() {
        boolean valid = letterValidator.isValidCity("");
        Assertions.assertThat(valid).isFalse();
    }

    /**
     * Unit Test zu Aufgabe 5
     */
    @Disabled
    @Test
    public void invalidNullCity() {
        boolean valid = letterValidator.isValidCity(null);
        Assertions.assertThat(valid).isFalse();
    }

    /**
     * Unit Test zu Aufgabe 6
     */
    @Disabled
    @Test
    public void validLetter() {
        Letter letter = new Letter.Builder().content("gültiger Content").city("Zürich").plz(ZH_PLZ).build();
        boolean valid = letterValidator.isValid(letter);
        Assertions.assertThat(valid).isTrue();
    }

    /**
     * Unit Test zu Aufgabe 6
     */
    @Disabled
    @Test
    public void invalidLetterBecauseOfContent() {
        Letter letter = new Letter.Builder().content("").city("Zürich").plz(ZH_PLZ).build();
        boolean valid = letterValidator.isValid(letter);
        Assertions.assertThat(valid).isFalse();
    }

    /**
     * Unit Test zu Aufgabe 6
     */
    @Disabled
    @Test
    public void invalidLetterBecauseOfCity() {
        Letter letter = new Letter.Builder().content("gültiger Content").city("Nirgendwo").plz(ZH_PLZ).build();
        boolean valid = letterValidator.isValid(letter);
        Assertions.assertThat(valid).isFalse();
    }

    /**
     * Unit Test zu Aufgabe 6
     */
    @Disabled
    @Test
    public void invalidLetterBecauseOfPlz() {
        Letter letter = new Letter.Builder().content("gültiger Content").city("Zürich").plz(0).build();
        boolean valid = letterValidator.isValid(letter);
        Assertions.assertThat(valid).isFalse();
    }

}
